'use strict'

const utilisateur = {
    prenom: 'Philippe',
    nom: 'Fleury', 
    sexe: 'H', 
    situationParticuliere: 'non', 
    dateNaissance: new Date(1982, 11, 10) }
const journeesAlcool = new Array()

function obtenirAge(date) { 
    let difference = Date.now() - date.getTime()
    let age = new Date(difference)
    return Math.abs(age.getUTCFullYear() - 1970)
}

// Le nombre de jours à analyser
let nbJours = parseInt(prompt('Combien de jours voulez-vous analyser ?'))
alert('Nous allons donc analyser ' + nbJours + ' jours.')

// Le tableau 
for (let i = 1; i <= nbJours; i++){
    const nbConsommationAlcool = parseInt(prompt("Entrez le nombre de consommation d'alcool pour jour " + i))
    journeesAlcool.push(nbConsommationAlcool)
}

function obtenirRecommandation_jour(){
    if(utilisateur.situationParticuliere == 'oui'){
        let maxAlcoolJour = 0
        return maxAlcoolJour
    }if (utilisateur.sexe == 'H') {
        let maxAlcoolJour = 3
        return maxAlcoolJour
    } else {
        let maxAlcoolJour = 2
        return maxAlcoolJour
    }
}

function obtenirRecommandation_semaine(){
    if(utilisateur.situationParticuliere == 'oui'){
        let maxAlcoolSemaine = 0
        return maxAlcoolSemaine
}   if (utilisateur.sexe == 'H') {
        let maxAlcoolSemaine = 15
        return maxAlcoolSemaine
}   else {
        let maxAlcoolSemaine = 10
        return maxAlcoolSemaine
    }
}

// Trouver consommation sur une semaine 
let somme = 0
for (let i = 0; i < journeesAlcool.length; i++) {
    somme = somme + journeesAlcool[i]
}

function obtenirMoyenneJour(){
    return somme / nbJours
}

function obtenirSommeHebdo(){
    return (somme / nbJours) * 7
}

const journeesZero = journeesAlcool.filter(function(nombre){
    return nombre == 0
})

function obtenirRatio(){
    return (journeesZero.length * 100) / nbJours
}

const journeesExces = journeesAlcool.filter(function(nombre){
    return nombre > obtenirRecommandation_jour();
})

function obtenirRatioExces(){
    return (journeesExces.length * 100) / nbJours
}

function recommandationRespectee(){
    if (Math.max(...journeesAlcool) <= obtenirRecommandation_jour() && obtenirSommeHebdo() <= obtenirRecommandation_semaine()) {
        return 'Vous respectez les recommandations'
    } else {
        return 'Vous ne respectez pas les recommandations'
    }
}
let maintenant_date = new Date().toLocaleString()

console.log(maintenant_date)
console.log(utilisateur.nom + ' , ' + utilisateur.prenom)
console.log('Âge : ' + obtenirAge(new Date(utilisateur.dateNaissance)) + ' ans')
console.log('Alcool : ')
console.log("Moyenne d'alcool par jour : " + obtenirMoyenneJour().toFixed(2))
console.log('Consommation sur une semaine : ' + obtenirSommeHebdo() + '  Recommandations : ' + obtenirRecommandation_semaine())
console.log("Le maximum en une journée est de : " + Math.max(...journeesAlcool) + '  Recommandations : ' + obtenirRecommandation_jour())
console.log('Ratio de journées excédants : ' + obtenirRatioExces().toFixed(2) + ' %')
console.log('Ratio de journées sans alcool : ' + obtenirRatio().toFixed(2) + ' %')
console.log(recommandationRespectee())